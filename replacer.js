'use strict';

// var proxy_global = [
//     's8.mkklcdnv8.com',
//     'bu.mkklcdnbuv1.com',
//     'img.mgicdn.com'
// ];

// var proxy_domain = [
//     's3.mkklcdnv3.com',
//     's3v3.mkklcdnv3.com',
//     's5.mkklcdnv5.com',
//     's5v5.mkklcdnv5.com',
//     's6.mkklcdnv6.com',
//     's6v6.mkklcdnv6.com',
//     's7.mkklcdnv7.com',
//     's71.mkklcdnv7.com',
//     's7v7.mkklcdnv7.com',
//     's41.mkklcdnv41.com',
//     's41v41.mkklcdnv41.com',
//     's51.mkklcdnv51.com',
//     's61.mkklcdnv61.com',
//     's61v61.mkklcdnv61.com',
//     's62.mkklcdnv61.com'
// ];

const path = './';
const fs = require('fs');

function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

var files = getFiles(path);
// https://img-proxy.imgfo.com/?referer=https://manganelo.com&url=https://avt.mkklcdnv6temp.com/24/z/11-1583484918.jpg
for (var i=0; i< files.length; i++) {
    if(files[i].endsWith('.json')) {
        var data = fs.readFileSync(files[i], 'utf8');
        var result = data
            .replace(/s62.mkklcdnv61temp.com/g, 's62.mkklcdnv6temp.com')
            .replace(/s61v61.mkklcdnv61temp.com/g, 's61v61.mkklcdnv6temp.com')
            .replace(/s61.mkklcdnv61temp.com/g, 's61.mkklcdnv6temp.com')
            .replace(/s51.mkklcdnv51temp.com/g, 's51.mkklcdnv6temp.com')
            .replace(/s41v41.mkklcdnv41temp.com/g, 's41v41.mkklcdnv6temp.com')
            .replace(/s41.mkklcdnv41temp.com/g, 's41.mkklcdnv6temp.com')
            .replace(/s7v7.mkklcdnv7temp.com/g, 's7v7.mkklcdnv6temp.com')
            .replace(/s71.mkklcdnv7temp.com/g, 's71.mkklcdnv6temp.com')
            .replace(/s7.mkklcdnv7temp.com/g, 's7.mkklcdnv6temp.com')
            .replace(/s6v6.mkklcdnv6temp.com/g, 's6v6.mkklcdnv6temp.com')
            .replace(/s6.mkklcdnv6temp.com/g, 's6.mkklcdnv6temp.com')
            .replace(/s5v5.mkklcdnv5temp.com/g, 's5v5.mkklcdnv6temp.com')
            .replace(/s5.mkklcdnv5temp.com/g, 's5.mkklcdnv6temp.com')
            .replace(/s3v3.mkklcdnv3temp.com/g, 's3v3.mkklcdnv6temp.com')
            .replace(/s3.mkklcdnv3temp.com/g, 's3.mkklcdnv6temp.com');
        fs.writeFile(files[i], result, 'utf8', function (err) {
            if (err) return console.log(err);
        }); 
    }  
}
