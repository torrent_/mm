'use strict';

// var proxy_global = [
//     's8.mkklcdnv8.com',
//     'bu.mkklcdnbuv1.com',
//     'img.mgicdn.com'
// ];

const path = './';
const fs = require('fs');

function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

var files = getFiles(path);
// https://img-proxy.imgfo.com/?referer=https://manganelo.com&url=https://avt.mkklcdnv6temp.com/24/z/11-1583484918.jpg

for (var i=0; i< files.length; i++) {
    if(files[i].endsWith('.json')) {
        var data = fs.readFileSync(files[i], 'utf8');
        if(data.indexOf('img.mgicdn.com') !== -1) {
            console.log(files[i]);
        } 
    }  
}
