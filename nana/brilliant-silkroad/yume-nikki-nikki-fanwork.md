---
title: "Yume Nikki Nikki - Fanwork"
date: 2020-06-09T06:36:14+07:00
draft: false

# post thumb
image: "https://avt.mkklcdnv6.com/19/y/14-1583490459.jpg"

# meta description
description: "Read complete manga Yume Nikki Nikki - Fanwork in english subtitle for free."

# taxonomies
categories: 
  - "Manga"
tags:
  - Completed
  - Manga

# post type
type: "post"
---

<!--more-->

|||
|---|---|
|**Title**| Yume Nikki Nikki - Fanwork
|**Genre**| Manga
|**Author**| Nana/brilliant Silkroad
|**Language**| English
|**Status**| Completed

## Description :
Madotsuki's diary, narrated from her own perspective.

## List Chapter :
  - <a href="/read?b=L2NvbGxlY3Rpb25zL25hbmEvYnJpbGxpYW50LXNpbGtyb2FkL3l1bWUtbmlra2ktbmlra2ktZmFud29yaw==&u=aHR0cHM6Ly9pbWdmby5jb20vZW1iZWQvP2NvbnRlbnQ9JTNEJTNEdmJnYW5OVEF1VnlMd2RHYUdGeTlqOXlhdWQyWm1GMmt0bHJhdGJtYTJrbWxyVXRiMWJXTDNsMkZrdHlicGJHTFhOVzUweHBZcGJHWW5KbUV2NWhieUwyZEdWV0Z6Y3ZieVlYYlM5eTl0NTBYeVpXYjNKUzkwTnZickxtWVdOWFJvNW5ha2JpYkdOeTluTTZMMGNIYUhS" rel="nofollow">Chapter 0</a>
